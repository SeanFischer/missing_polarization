assort_bar_df <- expand.grid(c("Music", "TV", "All Cultural",
                               "Political Concerns", "All Political"),
                             c("Party ID", "Ideology", "Gender", "Race", "Education"))
colnames(assort_bar_df) <- c("Network", "Measure")
assort_bar_df$Assortativity <- 
  c(assortativity(music_graph_proj, V(music_graph_proj)$pid),
    assortativity(tv_graph_proj, V(tv_graph_proj)$pid),
    assortativity(all_graph_proj, V(all_graph_proj)$pid),
    assortativity(issue_graph_proj, V(issue_graph_proj)$pid),
    assortativity(all_graph_policy_proj, V(all_graph_policy_proj)$pid),
    assortativity(music_graph_proj, V(music_graph_proj)$ideology),
    assortativity(tv_graph_proj, V(tv_graph_proj)$ideology),
    assortativity(all_graph_proj, V(all_graph_proj)$ideology),
    assortativity(issue_graph_proj, V(issue_graph_proj)$ideology),
    assortativity(all_graph_policy_proj, V(all_graph_policy_proj)$ideology),
    assortativity_nominal(music_graph_proj, as.factor(V(music_graph_proj)$gender)),
    assortativity_nominal(tv_graph_proj, as.factor(V(tv_graph_proj)$gender)),
    assortativity_nominal(all_graph_proj, as.factor(V(all_graph_proj)$gender)),
    assortativity_nominal(issue_graph_proj, as.factor(V(issue_graph_proj)$gender)),
    assortativity_nominal(all_graph_policy_proj, as.factor(V(all_graph_policy_proj)$gender)),
    assortativity_nominal(music_graph_proj, as.factor(V(music_graph_proj)$race)),
    assortativity_nominal(tv_graph_proj, as.factor(V(tv_graph_proj)$race)),
    assortativity_nominal(all_graph_proj, as.factor(V(all_graph_proj)$race)),
    assortativity_nominal(issue_graph_proj, as.factor(V(issue_graph_proj)$race)),
    assortativity_nominal(all_graph_policy_proj, as.factor(V(all_graph_policy_proj)$race)),
    assortativity_nominal(music_graph_proj, as.factor(V(music_graph_proj)$col_degree)),
    assortativity_nominal(tv_graph_proj, as.factor(V(tv_graph_proj)$col_degree)),
    assortativity_nominal(all_graph_proj, as.factor(V(all_graph_proj)$col_degree)),
    assortativity_nominal(issue_graph_proj, as.factor(V(issue_graph_proj)$col_degree)),
    assortativity_nominal(all_graph_policy_proj, as.factor(V(all_graph_policy_proj)$col_degree)))

ggplot(assort_bar_df, aes(x = Network, y = Assortativity, group = Measure)) +
  geom_bar(stat = "identity", position = "dodge", aes(fill = Measure)) +
  geom_text(aes(y = Assortativity + 0.01,
                 label = as.character(round(Assortativity, 3))),
             position = position_dodge(width = 1),
             size = 3) +
  fischeR::theme_saf() +
  theme(legend.position = "bottom")
